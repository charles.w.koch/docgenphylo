import os
import re

import config


def render_file(library, filepath):
	with open(filepath, 'r') as thefile:
		template = thefile.read()

	if not re.search(config.EXPORT_TAG, template):
		return None

	return render_template(library, filepath)


def render_template(library, filepath):
	template = ''
	try:
		with open(filepath, 'r') as thefile:
			template = thefile.read()

		inserts = re.findall(r'\{[^\t\n\r\f\v]+\}', template)

		insert_data = {}

		for entry in inserts:
			key = entry.strip('\n\t {}')
			replacestr = render_template(library, library[key])
			template = re.sub(re.escape(entry), replacestr, template)
	except OSError:
		print('ERROR: {} not found!'.format(filepath))

	return template


def catalog_templates(library, root):
	for dirpath, dirnames, filenames in os.walk(root):
		for filepath in filenames:
			splitname = filepath.split('.')
			file_ext = splitname[-1]
			filename = ''.join(splitname[:-1])
			if filename and file_ext in config.FILE_EXTS:
				fullpath = os.path.join(dirpath, filepath)
				dirpath_edited = ''.join(dirpath.split(os.sep)[1:])
				if dirpath_edited:
					dirpath_edited += '.'
				key = dirpath_edited.replace(os.sep, '.') + filename
				library[key] = fullpath


def main():
	os.makedirs(config.OUTPUT_DIR, exist_ok=True)

	library = {}
	catalog_templates(library, config.ROOT_DIR)

	for dirpath, dirnames, filenames in os.walk(config.ROOT_DIR):
		for filepath in filenames:
			splitname = filepath.split('.')
			file_ext = splitname[-1]
			filename = ''.join(splitname[:-1])
			if filename and file_ext in config.FILE_EXTS:
				fullpath = os.path.join(dirpath, filepath)
				outstr = render_file(library, fullpath)
				if outstr:
					outpath = os.path.join(config.OUTPUT_DIR, filepath)
					with open(outpath, 'w') as outfile:
						outfile.write(outstr)



if __name__ == '__main__':
	main()
