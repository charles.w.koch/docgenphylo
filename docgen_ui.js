var EMPTY_GLOBALS = {
	"species": {
		"id": 0,
		"cards": {},
		"untouched": true,
		"curr_image_data": null,
		"BLANK": {
			"card-id": -1,
			"common-name": "",
			"latin-name": "",
			"classification": "",
			"diet": "Autotroph",
			"diet-number": "1",
			"scale": 5,
			"points": 4,
			"points-class": "",
			"image-data": null,
			"image-url": "",
			"image-credit": "",
			"terrain1": "Forest",
			"terrain2": "",
			"terrain3": "",
			"climate1": "Warm",
			"climate2": "",
			"climate3": "",
			"effects": "",
			"fact": ""
		},
	},
	"event": {
		"id": 0,
		"cards": {},
		"untouched": true,
		"curr_image_data": null,
		"BLANK": {
			"card-id": -1,
			"common-name": "",
			"category": "Event",
			"diet": "",
			"diet-number": "",
			"scale": "",
			"type": "Man-Made",
			"action-class": "",
			"image-data": null,
			"image-url": "",
			"image-credit": "",
			"allterrain": false,
			"terrain-words": "",
			"terrain1": "",
			"terrain2": "",
			"terrain3": "",
			"allclimates": false,
			"climate-words": "",
			"climate1": "",
			"climate2": "",
			"climate3": "",
			"effects": "",
			"fact": ""
		},
	},
	"other": {
		"id": 0,
		"cards": {},
		"untouched": true,
		"curr_image_data": null,
		"BLANK": {
			"card-id": -1,
			"common-name": "",
			"sub-title": "",
			"marking": "",
			"image-data": null,
			"image-url": "",
			"image-credit": "",
			"left-text": "",
			"right-text": ""
		},
	}
};

var globals = {
	"species": {
		"id": 0,
		"cards": {},
		"untouched": true,
		"curr_image_data": null,
		"BLANK": {
			"card-id": -1,
			"common-name": "",
			"latin-name": "",
			"classification": "",
			"diet": "Autotroph",
			"diet-number": "1",
			"scale": 5,
			"points": 4,
			"points-class": "",
			"image-data": null,
			"image-url": "",
			"image-credit": "",
			"terrain1": "Forest",
			"terrain2": "",
			"terrain3": "",
			"climate1": "Warm",
			"climate2": "",
			"climate3": "",
			"effects": "",
			"fact": ""
		},
	},
	"event": {
		"id": 0,
		"cards": {},
		"untouched": true,
		"curr_image_data": null,
		"BLANK": {
			"card-id": -1,
			"common-name": "",
			"category": "Event",
			"diet": "",
			"diet-number": "",
			"scale": "",
			"type": "Man-Made",
			"action-class": "",
			"image-data": null,
			"image-url": "",
			"image-credit": "",
			"allterrain": false,
			"terrain-words": "",
			"terrain1": "",
			"terrain2": "",
			"terrain3": "",
			"allclimates": false,
			"climate-words": "",
			"climate1": "",
			"climate2": "",
			"climate3": "",
			"effects": "",
			"fact": ""
		},
	},
	"other": {
		"id": 0,
		"cards": {},
		"untouched": true,
		"curr_image_data": null,
		"BLANK": {
			"card-id": -1,
			"common-name": "",
			"sub-title": "",
			"marking": "",
			"image-data": null,
			"image-url": "",
			"image-credit": "",
			"left-text": "",
			"right-text": ""
		},
	}
};

const DIET_VALUES = {
	"Photosynthetic": 2,
	"Autotroph": 2,
	"Herbivore": 4,
	"Carnivore": 7,
	"Omnivore": 3,
	"Special": 6,
};

const DIET_NUMBER = {
	"None": "",
	"Photosynthetic": "1",
	"Autotroph": "1",
	"Herbivore": "2",
	"Carnivore": "3",
	"Omnivore": "3",
	"Special": "*",
};

const CLICK_FUNCS = {
	'#import_sample': importDefaultCards,
	'#import_json': importCardsJson,
	'#import_csv': importCardsCsv,
	'#export_json': exportCardsJson,
	'#export_csv': exportCardsCsv,
	'#print_species': printSpeciesCards,
	'#print_events': printEventCards,
	'#print_other': printOtherCards,
	'#delete-button': deleteCards,
	'#help-button': toggleHelp,
	'#info-button': getInfo,
};

const DB_NAME = 'phylo';
const DB_VERSION = 1;
var db;	// this is how we will interact with the database


///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// INDEXED DB FUNCTIONS
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------
function openDb() {
	// Let's ask for a database matching the name and version given
	console.log('Opening DB...');
	var request = indexedDB.open(DB_NAME, DB_VERSION);
	// Callbacks that the request will use
	request.onsuccess = function(event) {
		// if the database is opened successully,
		// let's set our global 'db' variable to that connection
		db = request.result;
		console.log("Opened " + DB_NAME + " v" + DB_VERSION + " database connection");
		// load the data from the DB to our globals variable
		for(const key in globals) {
			loadFromStorage(key);
		}
	};
	request.onerror = function(event) {
		// if the open fails, let's output the error
		console.error("Failed to open " + DB_NAME + " v" + DB_VERSION + ": ", event.target.errorCode);
	};
	request.onupgradeneeded = function(event) {
		// this is called whenever a new version of the database is created
		// i.e. when we first create it, or we upgrade it (think like a Django migration)
		console.log("Upgrading "+ DB_NAME + " database to version " + DB_VERSION);

		var connection = request.result;

		if (event.oldVersion < 1) {
			// db is new - create v1 schema
			try {
				var store = connection.createObjectStore('globals');
				// fill in starting data
				for(const key in globals) {
					store.put(EMPTY_GLOBALS[key], key);
				}
			} catch (ConstraintError) {
				// do nothing
			}
		}
	};
}

//----------------------------------------------------------------------------------------
function getTypeKey(typeKey) {
	// To access a specific card list, we need to get its object from the DB.
	var tx = db.transaction('globals', 'readonly');
	return tx.objectStore('globals').get(typeKey);
}

//----------------------------------------------------------------------------------------
function writeTypeKey(typeKey) {
	// Write a specific object to the database
	var tx = db.transaction('globals', 'readwrite');
	return tx.objectStore('globals').put(globals[typeKey], typeKey);
}

//----------------------------------------------------------------------------------------
function writeBlankTypeKey(typeKey) {
	// Write a specific object to the database
	var tx = db.transaction('globals', 'readwrite');
	return tx.objectStore('globals').put(EMPTY_GLOBALS[typeKey], typeKey);
}

//----------------------------------------------------------------------------------------
function deleteTypeKey(typeKey) {
	var tx = db.transaction('globals', 'readwrite');
	var request = tx.objectStore('globals').delete(typeKey);
	request.onsuccess = function(event) {
		console.log("Key '" + typeKey  + "' deleted");
		var writeReq = writeBlankTypeKey(typeKey);
		writeReq.onsuccess = function(event) {
			// nothing, it's all good!
		};
		writeReq.onerror = function(event) {
			console.error("writeTypeKey:", event.target.errorCode);
		};
	};
	request.onerror = function(event) {
		console.error("deleteTypeKey:", event.target.errorCode);
	};
}

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// WINDOW ON LOAD
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

window.onload = function()
{
	// let's get the database ready
	openDb();
	
	$('.help-trigger').addClass('invisible');
	//attach event listeners to toolbar buttons
	for(const key in CLICK_FUNCS) {
		$(key).on('click', function() {
			CLICK_FUNCS[key]();
		});
	}

	// clear all import file loaders when clicking the Import toolbar button
	$("#import-button").click(function() {
		$("#json_file").val(null);
		for(const typeKey in globals) {
			$("#"+typeKey+"_csv").val(null);
		}
	});

	for(const typeKey in globals) {
		//prepare cards
		loadBlankCard(typeKey)
		prepareInputs(typeKey);

		// set event listeners
		$('#add-'+typeKey).on('click', function() {
			if(!globals[typeKey].untouched) {
				var savenow = confirm('This card has not been saved yet! Do you want to save it now?');
				if(savenow) {
					saveCard(typeKey);
				}
			}
			$('.cardlist-item.'+typeKey).removeClass('selected');
			loadBlankCard(typeKey)
		});
		$('#save-'+typeKey).on('click', function() {
			saveCard(typeKey);
		});
		$('#delete-'+typeKey).on('click', function() {
			deleteCard(typeKey);
		});
	}
}

//----------------------------------------------------------------------------------------
function loadFromStorage(typeKey) {
	try {
		var request = getTypeKey(typeKey);
		request.onsuccess = function() {
			globals[typeKey] = request.result;
			try {
				globals[typeKey].untouched = true;
				renderCardList(typeKey);
			} catch {
				console.log('Failed to load '+typeKey+' cards!');
				globals[typeKey].cards = {};
				globals[typeKey].id = 0;
			}
		}
		request.onerror = function() {
			console.log('Failed to load '+typeKey+' cards!');
			globals[typeKey].cards = {};
			globals[typeKey].id = 0;
		}

	} catch {
		console.log('Failed to load '+typeKey+' cards!');
		globals[typeKey].cards = {};
		globals[typeKey].id = 0;
	}
}

//----------------------------------------------------------------------------------------
function saveToStorage(typeKey) {
	var request = writeTypeKey(typeKey);
	request.onsuccess = function(event) {
		// nothing, it's all good!
	};
	request.onerror = function(event) {
		console.error("saveToStorage:", event.target.errorCode);
	};
}

//----------------------------------------------------------------------------------------
function deleteFromStorage(typeKey) {
	deleteTypeKey(typeKey);
	globals[typeKey].cards = {};
	globals[typeKey].id = 0;
}




///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// TOOLBAR CODE
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------
function importCardsJson() {
	var dataLoader = document.getElementById("json_file");
	// read each file
	for(const dfile of dataLoader.files) {
		let dreader = new FileReader();
		dreader.onload = function() {
			var newdata = JSON.parse(dreader.result);
			readJsonData(newdata);
		}
		dreader.readAsText(dfile);
	}
}


//----------------------------------------------------------------------------------------
function readJsonData(newdata) {
	for(const typeKey in globals) {
		for(cardid in newdata[typeKey+'_cards']) {
			globals[typeKey].id += 1;
			globals[typeKey].cards[globals[typeKey].id] = newdata[typeKey+'_cards'][cardid];
			globals[typeKey].cards[globals[typeKey].id]['card-id'] = globals[typeKey].id;
		}
		saveToStorage(typeKey);
		resetEditor(typeKey);
	}
}


//----------------------------------------------------------------------------------------
function importCardsCsv() {
	for(const typeKey in globals) {
		try {
			// read each CSV
			let card_loader = document.getElementById(typeKey+"_csv");
			for(const card_file of card_loader.files) {
				let card_reader = new FileReader();
				card_reader.onload = function(){
					readCsv(typeKey, card_reader.result);
				}
				card_reader.readAsText(card_file);
			}
		} catch {
			console.error('Failed to load '+typeKey+' CSV file');
		}
	}
}


//----------------------------------------------------------------------------------------
function readCsv(typeKey, incoming) {
	let card_list = d3.csvParse(incoming);
	for(const obj of card_list) {
		globals[typeKey].id += 1;
		newobj = {}
		for(const key in globals[typeKey].BLANK) {
			if( obj[key] !== undefined ) {
				newobj[key] = obj[key];
			} else {
				newobj[key] = globals[typeKey].BLANK[key];
			}
		}	
		newobj['card-id'] = globals[typeKey].id;

		// do extra work per card type
		if( typeKey == 'species' ) { globals[typeKey].cards[globals[typeKey].id] = adjustSpeciesCard(newobj); }
		else if( typeKey == 'event' ) { globals[typeKey].cards[globals[typeKey].id] = adjustEventCard(newobj); }
		else if( typeKey == 'other' ) { globals[typeKey].cards[globals[typeKey].id] = adjustOtherCard(newobj); }
	}

	saveToStorage(typeKey);
	resetEditor(typeKey);
}


//----------------------------------------------------------------------------------------
function importDefaultCards() {
	var importnow = confirm('If you import the sample card set, the current card set will be lost.')
	if( importnow ) {
		executeDeleteAllCards();
		readJsonData(phylo_sample_data);
	}
}


//----------------------------------------------------------------------------------------
function exportCardsJson() {
	let filename = prompt('Please provide a name for the JSON export file.');
	if( filename !== undefined && filename !== null ) {
		var newdata = {
			"species_cards": globals['species'].cards,
			"species_id": globals['species'].id,
			"event_cards": globals['event'].cards,
			"event_id": globals['event'].id,
			"other_cards": globals['other'].cards,
			"other_id": globals['other'].id
		};
		var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(newdata));
	    downloadObject(dataStr, filename+".json");
	}
}


//----------------------------------------------------------------------------------------
function downloadObject(dataStr, exportName) {
	var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href",     dataStr);
    downloadAnchorNode.setAttribute("download", exportName);
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
}


//----------------------------------------------------------------------------------------
function exportCardsCsv() {
	let filename = prompt('Please provide a name for the CSV export files.');
	if( filename !== undefined && filename !== null ) {
		for(const typeKey in globals) {
			var card_data = Object.values(globals[typeKey].cards);
			var card_str = d3.csvFormat(card_data);
			var dataStr = "data:text;charset=utf-8," + encodeURIComponent(card_str);
		    downloadObject(dataStr, filename+"_"+typeKey+".csv");
		}
	}
}


//----------------------------------------------------------------------------------------
function printSpeciesCards() {
	printCards('species', 'phylo_species');
}

function printEventCards() {
	printCards('event', 'phylo_events');
}

function printOtherCards() {
	printCards('other', 'phylo_other');
}

function printCards(typeKey, config) {
	data = Object.values(globals[typeKey].cards);
	var print_backs = $('#print-card-backs-div').hasClass('checked');
	var collated = $('#print-collated-div').hasClass('checked');
	phyloRenderAndGo(data, phylo_configs[config], print_backs, collated);
}


//----------------------------------------------------------------------------------------
function deleteCards() {
	var deletenow = confirm('Delete all cards and start with a new blank card set?')
	if(deletenow) {
		executeDeleteAllCards();
		for(const typeKey in globals) {
			resetEditor(typeKey);
		}
	}
}


//----------------------------------------------------------------------------------------
function executeDeleteAllCards() {
	for(const typeKey in globals) {
		// reset all species data
		deleteFromStorage(typeKey);
	}
}


//----------------------------------------------------------------------------------------
function toggleHelp() {
	if($('.help-trigger').hasClass('invisible')) {
		$('.help-trigger').removeClass('invisible');
		open_toast('help-active');
	} else {
		$('.help-trigger').addClass('invisible');
	}
}


//----------------------------------------------------------------------------------------
function getInfo() {

}


//----------------------------------------------------------------------------------------
function loadImageFromFile(loader, callback) {
		myfile = loader.files[0];
		reader = new FileReader();
		reader.onload = callback
		reader.readAsDataURL(myfile);
}






///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// GENERIC CARD FUNCTIONS
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------
function prepareInputs(typeKey) {
	// prepare the normal inputs
	for(const key in globals[typeKey].BLANK) {
		$("#"+typeKey+"-"+key).on('change', function() {
			// special logic for species
			if(typeKey == 'species') {
				$('#species-recommended').val(calculatePoints());
				$('#species-diet-number').val(calculateDietNumber('species'));
			} else if(typeKey == 'event') {
				$('#event-diet-number').val(calculateDietNumber('event'));
			}
			// reset and render
			globals[typeKey].untouched = false;
			renderCard(typeKey);
		});
	}

	// special diet input preparation
	// if(typeKey == 'event' || typeKey == 'species') {
	// 	$('#'+typeKey+'-diet').on('change', function() {
	// 		console.log($('#'+typeKey+'-diet').val());
	// 		if ($('#'+typeKey+'diet').val() == "Special") {
	// 			$('#'+typeKey+'-diet-number').prop('disabled', false);
	// 		} else {
	// 			$('#'+typeKey+'-diet-number').prop('disabled', true);
	// 		}
	// 	});
	// }

	// special event input preparation
	if(typeKey == 'event'){
		$("#event-allterrain").on('click', function() {
			globals['event'].untouched = false;
			renderCard(typeKey);
		});
		$("#event-allclimates").on('click', function() {
			globals['event'].untouched = false;
			renderCard(typeKey);
		});
	}

	// prepare the image uploader
	$("#"+typeKey+"-image-file").on('change', function() {
		loader = $(this)[0]
		loadImageFromFile(loader, function() {
			globals[typeKey].curr_image_data = reader.result;
			if(typeKey == 'species') {
				$('#species-recommended').val(calculatePoints());
				$('#species-diet-number').val(calculateDietNumber('species'));
			} else if(typeKey == 'event') {
				$('#event-diet-number').val(calculateDietNumber('event'));
			}
			$('#'+typeKey+'-image-url').val('');
			$('#'+typeKey+'-image-credit').val('');
			globals[typeKey].untouched = false;
			renderCard(typeKey);
		});
	});
	// make the image URL text field delete the image uploaded
	$('#'+typeKey+'-image-url').off('change').on('change', function() {
		$("#'+typeKey+'-image-file").val(null);
		$('#'+typeKey+'-image-credit').val('');
		globals[typeKey].curr_image_data = null;
		if(typeKey == 'species') {
			$('#species-recommended').val(calculatePoints());
			$('#species-diet-number').val(calculateDietNumber('species'));
		} else if(typeKey == 'event') {
			$('#event-diet-number').val(calculateDietNumber('event'));
		}
		globals[typeKey].untouched = false;
		renderCard(typeKey);
	});
}

//----------------------------------------------------------------------------------------
function resetEditor(typeKey) {
	loadBlankCard(typeKey)
	renderCardList(typeKey);
}


//----------------------------------------------------------------------------------------
function renderCardList(typeKey) {
	$('#'+typeKey+'-cardlist').empty();
	for(const key in globals[typeKey].cards) {
		var node = $('<div></div>');
		node.attr('id', typeKey+'-'+key);
		node.addClass('list-item cardlist-item '+typeKey);
		node.html(globals[typeKey].cards[key]["common-name"]);
		node.on('click', function() {
			selectCard(typeKey, $(this).attr('id'));
		});
		$('#'+typeKey+'-cardlist').append(node);
	}
}


//----------------------------------------------------------------------------------------
function selectCard(typeKey, full_id) {
	var savenow = false;
	if(!globals[typeKey].untouched) {
		savenow = confirm('This card has not been saved yet! Do you want to save it now?');
		if(savenow) {
			saveCard(typeKey);
		}
	}
	key = full_id.substring(typeKey.length+1);
	$('.cardlist-item.'+typeKey).removeClass('selected');
	var el = $('#'+full_id);
	el.addClass('selected');
	loadCard(typeKey, globals[typeKey].cards[key]);
}


//----------------------------------------------------------------------------------------
function loadBlankCard(typeKey) {
	loadCard(typeKey, globals[typeKey].BLANK);
}


//----------------------------------------------------------------------------------------
function loadCard(typeKey, card) {
	// set inital values from data provided
	$("#"+typeKey+"-image-file").val(null);
	for(const key in card) {
		var value = card[key]
		try {
			if($("#"+typeKey+"-"+key).length) {
				$("#"+typeKey+"-"+key).val(value);
			}
		} catch {
			// do nothing
		}
	}
	// do extra work per card type
	if( typeKey == 'species' ) { loadExtraSpecies(card); }
	else if( typeKey == 'event' ) { loadExtraEvent(card); }
	else if( typeKey == 'other' ) { loadExtraOther(card); }
	// final steps
	globals[typeKey].curr_image_data = card['image-data'];
	globals[typeKey].untouched = true;
	renderCard(typeKey);
	// extra work in the final steps
	if( typeKey == 'species' ) { loadFinalSpecies(card); }
	else if( typeKey == 'event' ) { loadFinalEvent(card); }
	else if( typeKey == 'other' ) { loadFinalOther(card); }
}


//----------------------------------------------------------------------------------------
function jsonifyData(typeKey, card) {
	if (card === undefined || card === null){
		card = Object.assign({}, globals[typeKey].BLANK);
	}
	for(const key in globals[typeKey].BLANK) {
		var val = $("#"+typeKey+"-"+key).val();
		card[key] = val;
	}
	if(card['common-name'].trim() == "") {
		card['common-name'] = "Unnamed Card";
	}
	card['image-data'] = globals[typeKey].curr_image_data;
	// do extra work per card type
	if( typeKey == 'species' ) { card = adjustSpeciesCard(card); }
	else if( typeKey == 'event' ) { card = adjustEventCard(card); }
	else if( typeKey == 'other' ) { card = adjustOtherCard(card); }

	return card;
}

function cleanAndSaveAll() {
	// this is never used in the actual execution, it's just to clean up our sample data
	for(const typeKey in globals) {
		for(const cardid in globals[typeKey].cards) {

			let card = globals[typeKey].cards[cardid];
			// species fixes
			if( typeKey == 'species' ) {
				if(card['points'] != 1) {
					card['points-class'] = "s";
				} else {
					card['points-class'] = "";
				}
				card['diet-number'] = calculateDietNumber('species', card);
			}
		}
		saveToStorage(typeKey);
	}
}


//----------------------------------------------------------------------------------------
function renderCard(typeKey) {
	let card = jsonifyData(typeKey);
	if( typeKey == 'species' ) {
		renderSpeciesTemplate($('.viewer .template.species-card')[0], card);
	} else if( typeKey == 'event' ) {
		renderEventTemplate($('.viewer .template.event-card')[0], card);
	} else if( typeKey == 'other' ) {
		renderOtherTemplate($('.viewer .template.other-card')[0], card);
	}
}


//----------------------------------------------------------------------------------------
function saveCard(typeKey) {
	var card = {};
	var cardid = -1;
	if($('#'+typeKey+'-card-id').val() < 0) {
		card = Object.assign({}, globals[typeKey].BLANK);
		globals[typeKey].id += 1;
		$('#'+typeKey+'-card-id').val(globals[typeKey].id);
		card["card-id"] = globals[typeKey].id;
		globals[typeKey].cards[globals[typeKey].id] = card;
		cardid = globals[typeKey].id;
	} else {
		cardid = $('#'+typeKey+'-card-id').val()
		card = globals[typeKey].cards[cardid];
	}

	globals[typeKey].cards[cardid] = jsonifyData(typeKey, card);
	globals[typeKey].untouched = true;

	saveToStorage(typeKey);
	resetEditor(typeKey);
}


//----------------------------------------------------------------------------------------
function deleteCard(typeKey) {
	proceed = confirm('Are you sure you want to delete this card?');
	if( proceed ) {
		var id = $('#'+typeKey+'-card-id').val();
		delete globals[typeKey].cards[id];
		saveToStorage(typeKey);
		resetEditor(typeKey);
	}
}




///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// SPECIES CARD CODE
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------
function calculatePoints(card) {
	// calculate the recommended point value of a card
	var diet = 0;
	var terrain_bonus = 1;
	var climate_bonus = 1;
	if( card !== undefined && card !== null ) {
		diet = DIET_VALUES[card["diet"]];
		if(card["terrain2"] != "") {
			terrain_bonus -= 1;
		}
		if(card["terrain3"] != "") {
			terrain_bonus -= 1;
		}
		if(card["climate2"] != "") {
			climate_bonus -= 1;
		}
		if(card["climate3"] != "") {
			climate_bonus -= 1;
		}
	} else {
		diet = DIET_VALUES[$('#species-diet').val()];
		if($('#species-terrain2').val() != "") {
			terrain_bonus -= 1;
		}
		if($('#species-terrain3').val() != "") {
			terrain_bonus -= 1;
		}
		if($('#species-climate2').val() != "") {
			climate_bonus -= 1;
		}
		if($('#species-climate3').val() != "") {
			climate_bonus -= 1;
		}
	}
	return diet + terrain_bonus + climate_bonus;
}

//----------------------------------------------------------------------------------------
function calculateDietNumber(typeKey, card) {
	if ($('#'+typeKey+'-diet').val() == "Special") {
		$('#'+typeKey+'-diet-number').prop('disabled', false);
	} else {
		$('#'+typeKey+'-diet-number').prop('disabled', true);
	}

	if( card !== undefined && card !== null ) {
		if( card['diet'] != 'Special') {
			return DIET_NUMBER[card['diet']];
		}
		if( card['diet-number'] !== undefined && card['diet-number'] !== null && card['diet-number'] != "") {
			return card['diet-number'];
		}
	}

	var diet = $('#'+typeKey+'-diet').val();
	if( diet != 'Special') {
		return DIET_NUMBER[diet];
	}

	var diet_num = $('#'+typeKey+'-diet-number').val()
	if( diet_num !== undefined && diet_num !== null ) {
		return $('#'+typeKey+'-diet-number').val();
	}

	return "";
}


//----------------------------------------------------------------------------------------
function loadExtraSpecies(card) {
	// extra processing necessary for load
	$('#species-diet-number').val(calculateDietNumber('species', card));
}


//----------------------------------------------------------------------------------------
function loadFinalSpecies(card) {
	// processing required after load
	$('#species-recommended').val(calculatePoints(card));
}


//----------------------------------------------------------------------------------------
function adjustSpeciesCard(card) {
	// adjustments made during the data JSONify
	if(card['points'] != 1) {
		card['points-class'] = "s";
	} else {
		card['points-class'] = "";
	}
	card['diet-number'] = calculateDietNumber('species', card);
	return card;
}




///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// EVENT CARD CODE
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------
function loadExtraEvent(card) {
	// extra processing required during load
	if( card['allterrain']) {
		$('#event-allterrain-div').addClass('checked');
	} else {
		$('#event-allterrain-div').removeClass('checked');
	}
	if( card['allclimates']) {
		$('#event-allclimates-div').addClass('checked');
	} else {
		$('#event-allclimates-div').removeClass('checked');
	}

	$('#event-diet-number').val(calculateDietNumber('event', card));
}


//----------------------------------------------------------------------------------------
function loadFinalEvent(card) {
	// no final processing necessary after load
}


//----------------------------------------------------------------------------------------
function adjustEventCard(card) {
	// adjustments made during the data JSONify
	card['allterrain'] = $('#event-allterrain-div').hasClass('checked');
	card['allclimates'] = $('#event-allclimates-div').hasClass('checked');

	if(card['diet'] != "") {
		card['diet-img'] = "img/phylo/"+card['diet']+".png"
	} else {
		card['diet-img'] = "";
	}
	if(card['allterrain']) {
		card['terrain1'] = "All";
		card['terrain2'] = "";
		card['terrain3'] = "";
	} else {
		card['terrain-words'] = "";
	}

	card['diet-number'] = calculateDietNumber('event', card);

	return card;
}




///////////////////////////////////////////////////////
///////////////////////////////////////////////////////
// OTHER CARD CODE
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------
function loadExtraOther(card) {
	// no extra processing necessary for load
}

//----------------------------------------------------------------------------------------
function loadFinalOther(card) {
	// no final processing necessary after load
}

//----------------------------------------------------------------------------------------
function adjustOtherCard(card) {
	// no adjustments needed during JSONify
	return card
}

