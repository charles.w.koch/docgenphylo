
const basestyle = `
body {
	margin: 0;
}

div {
	overflow: hidden;
	box-sizing: border-box;
}

.US-Letter {
	height: 10.97in;
	width: 8.5in;
	padding: 0.25in;
	display: flex;
	flex-wrap: wrap;
	align-content: flex-start;
	page-break-after: always;
    justify-content: center;
}

.US-Letter-Landscape {
	height: 8.5in;
	width: 11in;
	padding: 0.25in;
	display: flex;
	flex-wrap: wrap;
	align-content: flex-start;
	page-break-after: always;
	justify-content: center;
}
`;

window.onload = function() {
}

function addNode(source, node1, tag, attr) {
	var newnode = source.createElement(tag);
	for (var key in attr) {
		var val = attr[key];
		newnode.setAttribute(key, val);
    }
    node1.appendChild(newnode);
    return newnode;
}

function phyloRender(data, front_config, back_config, collated, newdocument, frag) {
	var print_backs = true;
	if( front_config === undefined || front_config == null ) {
		console.error('Missing config file');
	}
	if( data === undefined || data == null ) {
		console.error('Missing data file');
	}
	if( back_config === undefined || back_config == null ) {
		print_backs = false;
	}

	let i = 0;

	while (i < data.length) {
		frag.appendChild(renderPage(data, i, front_config, front_config.templates_per_page, newdocument));
		if( print_backs && collated ) {
			frag.appendChild(renderPage(data, i, back_config, front_config.templates_per_page, newdocument));
		}
		i += front_config.templates_per_page;
	}

	if( print_backs && !collated ) {
		i = 0;
		while (i < data.length) {
			frag.appendChild(renderPage(data, i, back_config, front_config.templates_per_page, newdocument));
			i += front_config.templates_per_page;
		}
	}
}

function renderTemplate(obj, template) {
	let temp = template;
	newnode = document.querySelector('.card-templates-hidden .template.'+template).cloneNode(true);
	if (template == 'species-card') {
		newnode = renderSpeciesTemplate(newnode, obj);
	} else if (template == 'event-card') {
		newnode = renderEventTemplate(newnode, obj);
	} else if (template == 'other-card') {
		newnode = renderOtherTemplate(newnode, obj);
	} else if (template == 'card-back') {
		newnode = renderCardBackTemplate(newnode, obj);
	} else {
		console.log('ERROR: Incorrect template name provided!');
	}
	return newnode;
}

function getTitleSize(title) {
	if( title.length <= 11) {
		return "16pt";
	} else if( title.length <= 12) {
		return "15pt";
	} else if( title.length <= 13) {
		return "14pt";
	} else if( title.length <= 14) {
		return "13pt";
	} else if( title.length <= 15) {
		return "12pt";
	} else if( title.length <= 17) {
		return "11pt";
	} else {
		return "10pt";
	}
}

function getClimateIcon(data) {
	var climates = {
		'Hot': false,
		'Warm': false,
		'Cool': false,
		'Cold': false,
		'None': false
	}

	if(data['allclimates'] !== null &&
		data['allclimates'] !== undefined &&
		data['allclimates'] == true){
		return 'all_climates';
	}

	climates[data['climate1']] = true;
	climates[data['climate2']] = true;
	climates[data['climate3']] = true;

	// Hot
	if( climates.Hot && !climates.Warm && !climates.Cool && !climates.Cold ) {
		return 'hot'
	}
	// Hot Warm
	if( climates.Hot && climates.Warm && !climates.Cool && !climates.Cold ) {
		return 'hot_warm'
	}
	// Hot Warm Cool
	if( climates.Hot && climates.Cool && !climates.Cold ) {
		return 'hot_warm_cool'
	}
	// Hot Cool (all climates)
	if( climates.Hot && climates.Cold ) {
		return 'all_climates'
	}
	// Warm
	if( !climates.Hot && climates.Warm && !climates.Cool && !climates.Cold ) {
		return 'warm'
	}
	// Warm Cool
	if( !climates.Hot && climates.Warm && climates.Cool && !climates.Cold ) {
		return 'warm_cool'
	}
	// Warm Cool Cold
	if( !climates.Hot && climates.Warm && climates.Cold ) {
		return 'warm_cool_cold'
	}
	// Cool
	if( !climates.Hot && !climates.Warm && climates.Cool && !climates.Cold ) {
		return 'cool'
	}
	// Cool Cold
	if( !climates.Hot && !climates.Warm && climates.Cool && climates.Cold ) {
		return 'cool_cold'
	}
	// Cold
	if( !climates.Hot && !climates.Warm && !climates.Cool && climates.Cold ) {
		return 'cold'
	}
	// None
	if( !climates.Hot && !climates.Warm && !climates.Cool && !climates.Cold ) {
		return 'no_climate'
	}
}

function renderSpeciesTemplate(source, data) {
	source.querySelector('#card-species-common-name').innerHTML = data['common-name'];
	source.querySelector('#card-species-common-name').style['font-size'] = getTitleSize(data['common-name']);
	source.querySelector('#card-species-latin-name').innerHTML = data['latin-name'];
	source.querySelector('#card-species-classification').innerHTML = data['classification'];
	if( data['diet-number'] !== null && data['diet-number'] !== undefined ) {
		source.querySelector('#card-species-diet-number').innerHTML = data['diet-number'];
	} else {
		source.querySelector('#card-species-diet-number').innerHTML = "";
	}
	$(source.querySelector('#card-species-diet-number')).removeClass('Autotroph Herbivore Carnivore Omnivore Special Photosynthetic');
	$(source.querySelector('#card-species-diet-number')).addClass(data['diet']);
	source.querySelector('#card-species-scale').innerHTML = data['scale'];
	var pointstr = data['points'] + "";
	if( data['points'] != 1 ) {
		source.querySelector('#card-species-points-label').innerHTML = "POINTS";
	} else {
		source.querySelector('#card-species-points-label').innerHTML = "POINT";
	}
	source.querySelector('#card-species-points').innerHTML = pointstr;
	if(data['image-data'] !== null && data['image-data'] !== undefined){
		source.querySelector('#card-species-image-url').style["background-image"] = "url('"+data['image-data']+"')";
	} else if(data['image-url'] != "") {
		source.querySelector('#card-species-image-url').style["background-image"] = "url('"+data['image-url']+"')";
	} else {
		source.querySelector('#card-species-image-url').style["background-image"] = "url('img/2_0/placeholder.png')";
	}
	source.querySelector('#card-species-image-credit').innerHTML = data['image-credit'];
	source.querySelector('#card-species-terrain1').setAttribute('src', 'img/2_0/terrain_'+data['terrain1']+'.png');
	source.querySelector('#card-species-terrain2').setAttribute('src', 'img/2_0/terrain_'+data['terrain2']+'.png');
	source.querySelector('#card-species-terrain3').setAttribute('src', 'img/2_0/terrain_'+data['terrain3']+'.png');
	source.querySelector('#card-species-climate').setAttribute('src', 'img/2_0/'+getClimateIcon(data)+'.png');
	var outstr = data['effects'];
	outstr = outstr.replaceAll("\n", "<br />");
	source.querySelector('#card-species-effects').innerHTML = outstr;
	outstr = data['fact'];
	outstr = outstr.replaceAll("\n", "<br />");
	source.querySelector('#card-species-fact').innerHTML = outstr;

	return source;
}

function renderEventTemplate(source, data) {
	source.querySelector('#card-event-common-name').innerHTML = data['common-name'];
	source.querySelector('#card-event-common-name').style['font-size'] = getTitleSize(data['common-name']);
	source.querySelector('#card-event-category').innerHTML = data['category'];
	if( data['diet-number'] !== null && data['diet-number'] !== undefined ) {
		source.querySelector('#card-event-diet-number').innerHTML = data['diet-number'];
	} else {
		source.querySelector('#card-event-diet-number').innerHTML = "";
	}
	$(source.querySelector('#card-event-diet-number')).removeClass('Autotroph Herbivore Carnivore Omnivore Special Photosynthetic None');
	$(source.querySelector('#card-event-diet-number')).addClass(data['diet']);
	source.querySelector('#card-event-scale').innerHTML = data['scale'];
	source.querySelector('#card-event-type').innerHTML = data['type'];
	if(data['category'] == 'Action') {
		$(source.querySelector('#card-event-bottom-fill')).addClass('Action');
	} else {
		$(source.querySelector('#card-event-bottom-fill')).removeClass('Action');
	}
	if(data['image-data'] !== null && data['image-data'] !== undefined){
		source.querySelector('#card-event-image-url').style["background-image"] = "url('"+data['image-data']+"')";
	} else if(data['image-url'] != "") {
		$(source.querySelector('#card-event-image-url')).css("background-image", "url('"+data['image-url']+"')");
	} else {
		$(source.querySelector('#card-event-image-url')).css("background-image", "url('img/phylo/placeholder.png')");
	}
	source.querySelector('#card-event-image-credit').innerHTML = data['image-credit'];
	source.querySelector('#card-event-terrain1').setAttribute('src', 'img/2_0/terrain_'+data['terrain1']+'.png');
	source.querySelector('#card-event-terrain2').setAttribute('src', 'img/2_0/terrain_'+data['terrain2']+'.png');
	source.querySelector('#card-event-terrain3').setAttribute('src', 'img/2_0/terrain_'+data['terrain3']+'.png');
	source.querySelector('#card-event-climate').setAttribute('src', 'img/2_0/'+getClimateIcon(data)+'.png');
	var outstr = data['effects'];
	outstr = outstr.replaceAll("\n", "<br />");
	source.querySelector('#card-event-effects').innerHTML = outstr;
	outstr = data['fact'];
	outstr = outstr.replaceAll("\n", "<br />");
	source.querySelector('#card-event-fact').innerHTML = outstr;

	return source;
}

function renderOtherTemplate(source, data) {
	source.querySelector('#card-other-common-name').innerHTML = data['common-name'];
	source.querySelector('#card-other-common-name').style['font-size'] = getTitleSize(data['common-name']);
	source.querySelector('#card-other-sub-title').innerHTML = data['sub-title'];
	if( data['sub-title'] == "" ) {
		$(source.querySelector('#card-other-top-fill')).addClass('Action');
	} else {
		$(source.querySelector('#card-other-top-fill')).removeClass('Action');
	}
	source.querySelector('#card-other-marking').innerHTML = data['marking'];
	if(data['image-data'] !== null && data['image-data'] !== undefined){
		source.querySelector('#card-other-image-url').style["background-image"] = "url('"+data['image-data']+"')";
	} else if(data['image-url'] != "") {
		source.querySelector('#card-other-image-url').style["background-image"] = "url('"+data['image-url']+"')";
	} else {
		source.querySelector('#card-other-image-url').style["background-image"] = "url('img/phylo/placeholder.png')";
	}
	source.querySelector('#card-other-image-credit').innerHTML = data['image-credit'];
	source.querySelector('#card-other-left-text').innerHTML = data['left-text'];
	//source.querySelector('#card-other-right-text').innerHTML = data['right-text'];
	return source;
}

function renderCardBackTemplate(source, data) {
	return source;
}

function renderPage(data, index, config, templates_per_page, newdocument) {
	var node = newdocument.createElement('div');
	node.setAttribute('class', config.page_type);
	for(var i = index; i < index+templates_per_page; i++) {
		if( i >= data.length ) {
			break;
		}
		node.appendChild(renderTemplate(data[i], config.template))
	}
	return node;
}


function phyloRenderAndGo(data, config, print_backs, collated) {
	back_config = null;
	if( print_backs ) {
		back_config = phylo_configs['phylo_back']
	}
	// build the document
	frag = new DocumentFragment()

	let newwindow = window.open();
	let newdocument = newwindow.document;
	frag = new DocumentFragment()

	var html = addNode(newdocument, frag, 'html', {});
	var head = addNode(newdocument, html, 'head', {});
	node = addNode(newdocument, head, 'title', {});
	node.innerHTML = config.doc_title;
	node = addNode(newdocument, head, 'link', {
		"rel": "stylesheet",
		"type": "text/css",
		"href": "phylo-styles.css"
	});
	node = addNode(newdocument, head, 'link', {
		"rel": "stylesheet",
		"type": "text/css",
		"href": "cxdl/scss/main.css"
	});
	var style = addNode(newdocument, head, 'style', {});
	style.innerHTML = basestyle;
	var body = addNode(newdocument, html, 'body', {
		"style": "background-color: #fff;"
	});
	phyloRender(data, config, back_config, collated, newdocument, body);

	thediv = newdocument.createElement('div');
	thediv.appendChild(frag);

	newdocument.write(thediv.innerHTML);
	newdocument.close();

}


