
ROOT_DIR = 'templates' # the root directory that Quickie will start looking for templates in
FILE_EXTS = ['html', 'css'] # the file extensions that Quickie will consider when assembling templates
EXPORT_TAG = '<html>' # the value that must be present somewhere in the file for it to be exported
OUTPUT_DIR = '.' # the directory Quickie will export rendered template files to
