var phylo_configs = {
	"phylo_species": {
		"template": "species-card",
		"page_type": "US-Letter",
		"templates_per_page": 9,
		"doc_title": "Phylo Species"
	},
	"phylo_events": {
		"template": "event-card",
		"page_type": "US-Letter",
		"templates_per_page": 9,
		"doc_title": "Phylo Events"
	},
	"phylo_other": {
		"template": "other-card",
		"page_type": "US-Letter",
		"templates_per_page": 9,
		"doc_title": "Phylo Other"
	},
	"phylo_back": {
		"template": "card-back",
		"page_type": "US-Letter",
		"templates_per_page": 9,
		"doc_title": "Phylo Back"
	}
}