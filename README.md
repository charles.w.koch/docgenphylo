# DocGen Phylo

A tool for creating decks for a cooperative Phylo card game variant. Based on the original [DocGen](https://gitlab.com/charles.w.koch/docgen) using the [Cerrax Display Library](http://www.cerrax.com/pages/cxdl.html).

- You can try it out [here](https://thinkcolorful.org/docgen/phylo_docgen.html)!
- How to guide is [here](https://thinkcolorful.org/?p=1003)

# Contributing to DocGen Phylo

The main HTML page is assmebled via a Python script called Quickie. To assemble all of the templates into the single HTML page, run the following command:

```
python3 quickie.py
```

To adjust settings of the Quickie script, you can open the ```config.py``` and change the values.